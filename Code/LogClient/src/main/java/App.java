import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class App {

    private static final boolean DEBUG = true;

    private static final String FILTER = "@LOG_CLIENT";

    private String mSocketAddress;

    private int mSocketPort;

    private Applet mApplet;

    private Socket mSocket;

    public String getGreeting() {
        return "LOG CLIENT VERSION 1.0";
    }

    public App(String address, String port) {

        mApplet = new Applet();
        mSocketAddress = address;
        mSocketPort = Integer.parseInt(port);

        try {

            LOG("@LOG_CLIENT", "Connecting to " + mSocketAddress + ":" + mSocketPort);            
            mSocket = new Socket(mSocketAddress, mSocketPort);
            LOG("@LOG_CLIENT", "Connected");            
    
            InputStream in = mSocket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    
            OutputStream out = mSocket.getOutputStream();
            PrintStream printer = new PrintStream(out);
    
            String parcel;

            while(true) {
                printer.print("CLIENT_COMMAND_QUERY_ALL\n");
                while ((parcel = reader.readLine()) != null) {
                    LOG("@LOG_CLIENT", "parcel = " + parcel);
                    mApplet.process(parcel);
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (mSocket != null) {
                try {
                    mSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private static void LOG(String filter, String message) {
        if (DEBUG && (FILTER.endsWith(filter) || FILTER.equals("@"))) {
            System.out.println("DBG " + filter + " : " + message);
        }
    }      

    // $: gradle run -Pargs="192.168.0.103 29800"
    public static void main(String[] args) {
        App app = new App(args[0], args[1]);
        LOG("@LOG_CLIENT", app.getGreeting());
    }
}
