public class Applet {

    private static final boolean DEBUG = true;

    private static final String FILTER = "@LOG_CLIENT";

    public Applet() {
    }

    public void process(String parcel) {
        // do something with parcel here...
    }

    @SuppressWarnings("unused")
    private static void LOG(String filter, String message) {
        if (DEBUG && (FILTER.endsWith(filter) || FILTER.equals("@"))) {
            System.out.println("DBG " + filter + " : " + message);
        }
    }   
}