public class Applet {

    private static final boolean DEBUG = true;

    private static final String FILTER = "@ARTS_EMU";

    public final String SELECT_PPSE_COMMAND = "00A404000E325041592E5359532E444446303100";

    public final String SELECT_PPSE_RESPONSE = "6F32840E325041592E5359532E4444463031A520BF0C1D611B4F07A000000003101050105649534120434F4E544143544C4553539000";

    public final String SELECT_VISA_COMMAND = "00A4040007A000000003101000";

    public final String SELECT_VISA_RESPONSE = "6F438407A0000000031010A53850105649534120434F4E544143544C455353BF0C089F5A0510084008409F38189F66049F02069F03069F1A0295055F2A029A039C019F37049000";

    public final String GPO_COMMAND = "80A8";

    public final String GPO_RESPONSE = "777A9F5D0600000007182157134761739001010010D20122011234599999991F9F2701809F2608AED0D50A40DA699F9F3602005C820200209F6E04204000009F6C0200009F10201F220100A400000200112233445566778899AABBCCDDEEFF11223344556677885F3401005F200E4E616D6530696E205265636F72649000";

    public final String APDU_NOT_SUPPORTED = "6D00";

    private int mCounter;

    public Applet() {
        mCounter = 0;
    }

    public String process(String rawapdu) {
        
        String apduCommand = prepareApdu(rawapdu);        
        LOG("@ARTS_EMU", "Counter = " + (mCounter++));
        LOG("@ARTS_EMU", "APDU Command = " + apduCommand);
        
        // Simulate card delay for 300 ms
        try {
            Thread.sleep(300);
        } catch(InterruptedException exception) {
            exception.printStackTrace();
        }
        
        if (apduCommand.equals(SELECT_PPSE_COMMAND)) {
            LOG("@ARTS_EMU", "APDU Response = " + SELECT_PPSE_RESPONSE);       
            return prepareResponse(SELECT_PPSE_RESPONSE);  
        }
        
        if (apduCommand.equals(SELECT_VISA_COMMAND)) {
            LOG("@ARTS_EMU", "APDU Response = " + SELECT_VISA_RESPONSE);
            return prepareResponse(SELECT_VISA_RESPONSE);
        }
        
        if (apduCommand.startsWith(GPO_COMMAND)) {
            LOG("@ARTS_EMU", "APDU Response = " + GPO_RESPONSE);
            return prepareResponse(GPO_RESPONSE);
        }

        LOG("@ARTS_EMU", "APDU Response = " + APDU_NOT_SUPPORTED);
        return prepareResponse(APDU_NOT_SUPPORTED);
    }

    /**
     * Adding prefix come from POS Terminal before sending to ARTS.
     *
     * @param apdu from POS Terminal
     * @return prepared apdu
     */
    private static String prepareApdu(String apdu) {
        StringBuilder sb = new StringBuilder(apdu);
        sb.delete(0, 16);
        return sb.toString();
    }

    /**
     * Removing prefix come from ARTS before sending to POS Terminal
     *
     * @param response from ARTS
     * @return apdu response without prefix
     */
    private static String prepareResponse(String rawresponse) {
        StringBuilder sb = new StringBuilder();
        int lengthInt = rawresponse.length()/2;
        String lengthByte = Integer.toHexString(lengthInt);
        if (lengthInt < 0x10) sb.append("0").append(lengthByte).append("000000");
        else sb.append(lengthByte).append("000000");
        sb.append(rawresponse);
        return sb.toString();
    }

    private static void LOG(String filter, String message) {
        if (DEBUG && (FILTER.endsWith(filter) || FILTER.equals("@"))) {
            System.out.println("DBG " + filter + " : " + message);
        }
    }   
}