package com.visa.probesim.systemcrash;

import java.io.PrintWriter;
import java.io.StringWriter;

public class GlobalException extends RuntimeException {

    /**
     * Use this exception type to let global exception handler (App.java) log the stack trace and
     * inform user for quick debugging.
     *
     * @param exception from original exception
     */
    public static GlobalException build(Exception exception) {
        StringWriter stringWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stringWriter));
        return new GlobalException((new StringBuilder("<<").append(stringWriter).append(">>")).toString());
    }

    private GlobalException(String message) {
        super(message);
    }
}
