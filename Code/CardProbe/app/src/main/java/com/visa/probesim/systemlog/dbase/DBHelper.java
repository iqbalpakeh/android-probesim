package com.visa.probesim.systemlog.dbase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    /**
     *  Data base name
     */
    private static final String DATABASE_NAME = "probesim_log.db";

    /**
     *  Data base version
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * DBHelper single object reference
     */
    private static DBHelper mDBHelper;

    /**
     * Create single instance object and thread-safe
     *
     * @param context of application
     * @return single DBHelper object
     */
    public static synchronized DBHelper getInstance(Context context) {
        if (mDBHelper == null) mDBHelper = new DBHelper(context);
        return mDBHelper;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQL_CREATE_TRACE_TABLE = "CREATE TABLE " + DBContract.Trace.TABLE_NAME + " ("
                + DBContract.Trace.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBContract.Trace.COLUMN_TYPE + " TEXT NOT NULL, "
                + DBContract.Trace.COLUMN_TIMESTAMP + " TEXT NOT NULL, "
                + DBContract.Trace.COLUMN_CONTENT + " TEXT NOT NULL);";

        String SQL_CREATE_ERROR_TABLE = "CREATE TABLE " + DBContract.Error.TABLE_NAME + " ("
                + DBContract.Error.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBContract.Error.COLUMN_TYPE + " TEXT NOT NULL, "
                + DBContract.Error.COLUMN_TIMESTAMP + " TEXT NOT NULL, "
                + DBContract.Error.COLUMN_CONTENT + " TEXT NOT NULL);";

        db.execSQL(SQL_CREATE_TRACE_TABLE);
        db.execSQL(SQL_CREATE_ERROR_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // nothing until next db upgrade
    }
}
