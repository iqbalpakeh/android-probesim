package com.visa.probesim.comm;

import com.visa.probesim.systemcrash.GlobalException;

import java.util.concurrent.LinkedBlockingDeque;

public class IOBuffer {

    /**
     * Singleton instance reference
     */
    private static IOBuffer mIOBuffer;

    /**
     * Blocking Deque containing APDU Command. This buffer move
     * data from Terminal to ARTS.
     */
    private LinkedBlockingDeque<String> mAPDUCommandDeque;

    /**
     * Blocking Deque containing APDU Response. This buffer move
     * data from ARTS to Terminal.
     */
    private LinkedBlockingDeque<String> mAPDUResponseDeque;

    /**
     * Get singleton instance of IOBuffer
     *
     * @return IOBuffer singleton object
     */
    public synchronized static IOBuffer getInstance() {
        if (mIOBuffer == null) mIOBuffer = new IOBuffer();
        return mIOBuffer;
    }

    private IOBuffer() {
        mAPDUCommandDeque = new LinkedBlockingDeque<>();
        mAPDUResponseDeque = new LinkedBlockingDeque<>();
    }

    /**
     * Destroy IOBuffer. The object will be reclaimed by Garbage Collector.
     */
    public synchronized void destroyBuffer() {
        mIOBuffer = null;
    }

    /**
     * Clear IOBuffer.
     */
    public synchronized void clearBuffer() {
        mAPDUCommandDeque.clear();
        mAPDUResponseDeque.clear();
    }

    /**
     * Put packet to IOBuffer. This is blocking function. It waits
     * until queue is empty before put the packet on queue.
     *
     * @param packet contain apdu command from Terminal
     */
    public void putApduCommand(String packet) {
        try {
            mAPDUCommandDeque.put(packet);
        } catch (InterruptedException error) {
            throw GlobalException.build(error);
        }
    }

    /**
     * Take packet from IOBuffer. This is blocking function. It waits
     * until queue is not empty before take the packet from queue.
     *
     * @return packet contain apdu command from Terminal
     */
    public String takeApduCommand() {
        String packet;
        try {
            packet = mAPDUCommandDeque.take();
        } catch (InterruptedException error) {
            throw GlobalException.build(error);
        }
        return packet;
    }

    /**
     * Put packet to IOBuffer. This is blocking function. It waits
     * until queue is empty before put the packet on queue.
     *
     * @param packet contain SW coming from RATS
     */
    public void putApduResponse(String packet) {
        try {
            mAPDUResponseDeque.put(packet);
        } catch (InterruptedException error) {
            throw GlobalException.build(error);
        }
    }

    /**
     * Take packet from IOBuffer. This is blocking function. It waits
     * until queue is not empty before take the packet from queue.
     *
     * @return packet contain SW from RATS
     */
    public String takeApduResponse() {
        String packet;
        try {
            packet = mAPDUResponseDeque.take();
        } catch (InterruptedException error) {
            throw GlobalException.build(error);
        }
        return packet;
    }
}
