package com.visa.probesim.systemlog.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.visa.probesim.systemlog.dbase.DBContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Trace {

    /**
     * Trace Type consist of COMMAND type and RESPONSE type
     */
    private String mType;

    public static class Type {
        public static final String Command = "COMMAND";
        public static final String Response = "RESPONSE";
    }

    /**
     * Local time taken when trace log
     */
    private String mTimestamp;

    /**
     * APDU Command or APDU Response
     */
    private String mContent;

    /**
     * Private constructor. Only access by build() function.
     *
     * @param type of error
     * @param timestamp of error
     * @param content of error
     */
    private Trace(String type, String timestamp, String content) {
        this.mType = type;
        this.mTimestamp = timestamp;
        this.mContent = content;
    }

    /**
     * Build trace object to be stored to SQLite database
     *
     * @param type of trace
     * @param timestamp of trace
     * @param content of trace
     * @return trace object
     */
    public static Trace build(String type, String timestamp, String content) {
        return new Trace(type, timestamp, content);
    }

    /**
     * Build trace object from cursor object
     *
     * @param cursor trace object
     * @return trace object
     */
    public static Trace build(Cursor cursor) {
        return new Trace(
                cursor.getString(cursor.getColumnIndex(DBContract.Trace.COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(DBContract.Trace.COLUMN_TIMESTAMP)),
                cursor.getString(cursor.getColumnIndex(DBContract.Trace.COLUMN_CONTENT))
        );
    }

    /**
     * Build parcel object from trace object. Parcel object used to send information easily to
     * client reading db information.
     *
     * @return parcel format of trace object
     */
    public String parcel() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(mTimestamp));
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
        String timestamp = fmt.format(calendar.getTime());
        return (new StringBuilder(timestamp).append("|").append(mType).append("|").append(mContent)).toString();
    }

    /**
     * Store trace object to Trace Table on content provider
     *
     * @param context of application
     */
    public void store(Context context) {
        ContentValues values = new ContentValues();
        values.put(DBContract.Trace.COLUMN_TYPE, mType);
        values.put(DBContract.Trace.COLUMN_TIMESTAMP, mTimestamp);
        values.put(DBContract.Trace.COLUMN_CONTENT, mContent);
        context.getContentResolver().insert(DBContract.Trace.CONTENT_URI, values);
    }

    /**
     * Clear all trace data from table
     *
     * @param context of application
     */
    public static void clearTable(Context context) {
        context.getContentResolver().delete(DBContract.Trace.CONTENT_URI, null, null);
    }
}
