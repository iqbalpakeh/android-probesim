package com.visa.probesim.systemcrash;

import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.visa.probesim.R;
import com.visa.probesim.Util;

public class CrashActivity extends AppCompatActivity {

    public static final String TAG = "CrashActivity";

    private TextView mVersionText;

    private TextView mErrorText;

    private Button mCloseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash);
        Util.Log(TAG, "Handle un-catch exception");

        try {
            mVersionText = findViewById(R.id.version_name);
            mVersionText.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException error) {
            throw GlobalException.build(error);
        }

        mErrorText = findViewById(R.id.error_text);
        mErrorText.setMovementMethod(new ScrollingMovementMethod());
        mErrorText.setText(getIntent().getStringExtra("CRASH"));

        mCloseButton = findViewById(R.id.close_button);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
