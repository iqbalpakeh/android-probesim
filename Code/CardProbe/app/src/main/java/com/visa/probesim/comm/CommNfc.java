package com.visa.probesim.comm;

import android.content.Intent;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.visa.probesim.Util;
import com.visa.probesim.systemlog.LogStatus;
import com.visa.probesim.systemlog.model.Trace;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CommNfc extends HostApduService {

    /**
     * Class tag for debugging
     */
    public static final String TAG = "CommNfc";

    /**
     * Sets the amount of time an idle thread waits before terminating
     */
    private static final int KEEP_ALIVE_TIME = 1;

    /**
     * Sets the Time Unit to seconds
     */
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    /**
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    /**
     * A queue of Runnable objects
     */
    private final BlockingQueue<Runnable> mDecodeWorkQueue = new LinkedBlockingQueue<>();

    /**
     * Control the thread usage
     */
    private ThreadPoolExecutor mThreadPool;

    /**
     * Intent object
     */
    private Intent mIntent;

    /**
     * Flag used to check if the apdus exchange session is still active.
     */
    private boolean mActive;

    /**
     * Static class content broadcast status information
     */
    public static class Status {

        /**
         * Broadcast the status of CommSocket service
         */
        public static final String BROADCAST_STATUS = "com.visa.probesim.comm.CommNfc";

        /**
         * key : status
         */
        public static final String KEY_STATUS = "STATUS";

        /**
         * value : show indicator
         */
        public static final String STATUS_ACTIVE = "ACTIVE";

        /**
         * value : hide indicator
         */
        public static final String STATUS_IDLE = "IDLE";

        /**
         * volatile variable of current status
         */
        private static String mStatus = STATUS_IDLE;

        /**
         * Store current comm socket status to shared preference
         *
         * @param status of COMM Socket
         */
        public synchronized static void store(String status) {
            mStatus = status;
        }

        /**
         * Get current comm socket status.
         *
         * @return current status
         */
        public synchronized static String current() {
            return mStatus;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mActive = false;
        mIntent = new Intent(Status.BROADCAST_STATUS);
        mThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,
                NUMBER_OF_CORES,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                mDecodeWorkQueue
        );
    }

    /**
     * This method will be called when a command APDU has been received from a remote device. A
     * response APDU can be provided directly by returning a byte-array in this method. In general
     * response APDUs must be sent as quickly as possible, given the fact that the user is likely
     * holding his device over an NFC reader when this method is called.
     * <p>
     * <p class="note">If there are multiple services that have registered for the same AIDs in
     * their meta-data entry, you will only get called if the user has explicitly selected your
     * service, either as a default or just for the next tap.
     * <p>
     * <p class="note">This method is running on the main thread of your application. If you
     * cannot return a response APDU immediately, return null and use the {@link
     * #sendResponseApdu(byte[])} method later.
     *
     * @param apdu   The APDU that received from the remote device
     * @param extras A bundle containing extra data. May be null.
     * @return a byte-array containing the response APDU, or null if no response APDU can be sent
     * at this point.
     */
    @Override
    public byte[] processCommandApdu(byte[] apdu, Bundle extras) {

        /*
         * Logging APDU is not recommended for long running as it use a lot of resource. Use
         * only for debugging.
         ********************************************************************************************/

        if (CommSocket.Status.current().equals(CommSocket.Status.STATUS_CONNECTED)) {

            if (!mActive) {
                mActive = true;
                broadcastStatus(Status.STATUS_ACTIVE);
            }

            IOBuffer.getInstance().clearBuffer();

            String apduCommand = Util.ByteArrayToHexString(apdu);
            Util.Log(TAG, "APDU Command: " + apduCommand);
            if (LogStatus.current(this).equals(LogStatus.STATUS_LOG)) {
                Trace.build(Trace.Type.Command, Util.now(), apduCommand).store(this);
            }

            IOBuffer.getInstance().putApduCommand(apduCommand + "\n");
            Util.Log(TAG, "Process..");

            /*
             * To handle client processing data longer than 200 ms, returning apdu response will be
             * done later by different worker thread as mentioned by this API documentation saying
             *
             * "if cannot return response APDU immediately, return null and use the
             * sendResponseApdu() method later"
             */
            mThreadPool.execute(mApduResponseTask);
            return null;

        } else {
            Util.Log(TAG, "CommSocket is not Connected");
            return null;
        }
    }

    /**
     * Task to collect apdu response from IOBuffer
     */
    private Runnable mApduResponseTask = new Runnable() {
        @Override
        public void run() {
            String apduResponse = IOBuffer.getInstance().takeApduResponse();
            Util.Log(TAG, "APDU Response: " + apduResponse);
            if (LogStatus.current(CommNfc.this).equals(LogStatus.STATUS_LOG)) {
                Trace.build(Trace.Type.Response, Util.now(), apduResponse).store(CommNfc.this);
            }
            CommNfc.this.sendResponseApdu(Util.HexStringToByteArray(apduResponse));
        }
    };

    /**
     * Called if the connection to the NFC card is lost, in order to let the application know the
     * cause for the disconnection (either a lost link, or another AID being selected by the
     * reader).
     *
     * @param reason Either DEACTIVATION_LINK_LOSS or DEACTIVATION_DESELECTED
     */
    @Override
    public void onDeactivated(int reason) {
        if (reason == DEACTIVATION_LINK_LOSS) {
            Util.Log(TAG, "onDeactivated: DEACTIVATION_LINK_LOSS");
            mActive = false;
            broadcastStatus(Status.STATUS_IDLE);
        } else Util.Log(TAG, "onDeactivated: DEACTIVATION_DESELECTED");
    }

    /**
     * Send broadcast to main-activity showing the nfc service status to user
     *
     * @param status of socket server
     */
    private void broadcastStatus(String status) {
        CommNfc.Status.store(status);
        mIntent.putExtra(Status.KEY_STATUS, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
    }

}
