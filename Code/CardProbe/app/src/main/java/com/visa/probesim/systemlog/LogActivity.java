package com.visa.probesim.systemlog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.visa.probesim.R;
import com.visa.probesim.Util;

public class LogActivity extends AppCompatActivity {

    public static final String TAG = "LogActivity";

    public static final String EXTRA_IPADDRESS = "EXTRA_IPADDRESS";

    private TextView mSocketStatus;

    private TextView mIpAddressText;

    private String mIpAddress;

    private BroadcastReceiver mSocketReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra(LogService.Status.KEY_STATUS);
            Util.Log(TAG, "Socket server status = " + status);
            mSocketStatus.setText(status);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        startService(new Intent(LogActivity.this, LogService.class));

        mIpAddress = getIntent().getStringExtra(EXTRA_IPADDRESS);
        Util.Log(TAG, "mIpAddress = " + mIpAddress);

        Button closeButton = findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(LogActivity.this, LogService.class));
                LogActivity.super.onSupportNavigateUp();
            }
        });

        mSocketStatus = findViewById(R.id.socket_status);
        mIpAddressText = findViewById(R.id.ip_address);

        mSocketStatus.setText(LogService.Status.current());
        mIpAddressText.setText(new StringBuilder(mIpAddress).append(" : ").append(LogService.PORT_NUMBER));
    }

    @Override
    public boolean onSupportNavigateUp() {
        stopService(new Intent(LogActivity.this, LogService.class));
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(LogActivity.this, LogService.class));
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        instance.registerReceiver(mSocketReceiver, new IntentFilter(LogService.Status.BROADCAST_STATUS));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        instance.unregisterReceiver(mSocketReceiver);
    }
}
