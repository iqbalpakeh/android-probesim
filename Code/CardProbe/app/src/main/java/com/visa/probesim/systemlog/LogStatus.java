package com.visa.probesim.systemlog;

import android.content.Context;
import android.content.SharedPreferences;

public class LogStatus {

    /**
     * key : status
     */
    public static final String KEY_STATUS = "STATUS";

    /**
     * value : log apdus
     */
    public static final String STATUS_LOG = "LOG";

    /**
     * value : do not log apdus
     */
    public static final String STATUS_NO_LOG = "NO_LOG";

    /**
     * Name of the shared preference
     */
    public static final String SHARED_PREFERENCE = "LOG_PREFERENCE";

    /**
     * Store current comm socket status to shared preference
     *
     * @param context of application
     * @param status  of COMM Socket
     */
    public static void store(Context context, String status) {
        SharedPreferences appContext = context.getSharedPreferences(SHARED_PREFERENCE, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(KEY_STATUS, status);
        editor.apply();
    }

    /**
     * Get current comm socket status.
     *
     * @param context of application
     * @return current status
     */
    public static String current(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(SHARED_PREFERENCE, 0);
        return appContext.getString(KEY_STATUS, STATUS_NO_LOG);
    }
}
