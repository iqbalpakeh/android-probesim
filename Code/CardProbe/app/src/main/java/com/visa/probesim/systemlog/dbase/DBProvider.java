package com.visa.probesim.systemlog.dbase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DBProvider extends ContentProvider {

    /**
     * How to use ADB to debug SQLite3:
     *      https://stackoverflow.com/questions/18370219/how-to-use-adb-in-android-studio-to-view-an-sqlite-db
     *
     *********************************************************************************************************/

    /**
     * Responsible to select which SQL operation to be executed
     */
    private static final UriMatcher mUriMatcher = buildUriMatcher();

    /**
     * SQLite data base helper
     */
    private DBHelper mDBHelper;

    /**
     * Integer used to execute query for the whole Trace table
     */
    private static final int TRACE_ALL = 100;

    /**
     * Integer used to execute query for the whole Error table
     */
    private static final int ERROR_ALL = 200;

    /**
     * Build Uri Matcher for query
     *
     * @return matcher object
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority = DBContract.CONTENT_AUTHORITY;
        matcher.addURI(authority, DBContract.Trace.TABLE_NAME, TRACE_ALL);
        matcher.addURI(authority, DBContract.Error.TABLE_NAME, ERROR_ALL);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDBHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        switch (mUriMatcher.match(uri)) {
            case TRACE_ALL: {
                cursor = mDBHelper.getReadableDatabase().query(
                        DBContract.Trace.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case ERROR_ALL: {
                cursor = mDBHelper.getReadableDatabase().query(
                        DBContract.Error.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        Uri returnUri;
        switch (mUriMatcher.match(uri)) {
            case TRACE_ALL: {
                long id = db.insert(DBContract.Trace.TABLE_NAME, null, contentValues);
                if (id > 0) returnUri = DBContract.Trace.buildUri(id);
                else throw new android.database.SQLException("Failed to insert row into: " + uri);
                break;
            }
            case ERROR_ALL: {
                long id = db.insert(DBContract.Error.TABLE_NAME, null, contentValues);
                if (id > 0) returnUri = DBContract.Error.buildUri(id);
                else throw new android.database.SQLException("Failed to insert row into: " + uri);
                break;
            }
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        int match = mUriMatcher.match(uri);
        int numDeleted;
        switch (match) {
            case TRACE_ALL:
                numDeleted = db.delete(
                        DBContract.Trace.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + DBContract.Trace.TABLE_NAME + "'");
                break;
            case ERROR_ALL:
                numDeleted = db.delete(
                        DBContract.Error.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + DBContract.Error.TABLE_NAME + "'");
                break;
            default: throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return numDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
