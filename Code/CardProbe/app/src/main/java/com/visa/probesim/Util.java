package com.visa.probesim;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.visa.probesim.systemcrash.GlobalException;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class Util {

    /**
     * For easy debug log activation. Value must be false for release version
     */
    private static final boolean DEBUG = false;

    /**
     * Function to log important message to help debugging. Make sure this function comment out
     * before releasing apk
     *
     * @param tag debug filter
     * @param message debug message
     */
    public static void Log(String tag, String message) {
        if (DEBUG) Log.d(tag, message);
    }

    /**
     * Utility method to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    /**
     * Utility method to convert a hexadecimal string to a byte string.
     * <p>
     * <p>Behavior with input strings containing non-hexadecimal characters is undefined.
     *
     * @param s String containing hexadecimal characters to convert
     * @return Byte array generated from input
     * @throws java.lang.IllegalArgumentException if input length is incorrect
     */
    public static byte[] HexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }
        byte[] data = new byte[len / 2]; // Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            // Convert each character into a integer (base-16), then bit-shift into place
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Get local IP Address to be selected by Socket Client
     *
     * @return possible ip addresses
     */
    public static String getIpAddress() {
        String ipAddress = "";
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> internetAddress = networkInterface.getInetAddresses();
                while (internetAddress.hasMoreElements()) {
                    InetAddress inetAddress = internetAddress.nextElement();
                    String address = inetAddress.getHostAddress();
                    if (inetAddress.isSiteLocalAddress() && address.startsWith("192")) {
                        ipAddress = inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException error) {
            throw GlobalException.build(error);
        }
        return ipAddress;
    }

    /**
     * Warn user with toast message
     *
     * @param message for user
     */
    public static void warnUser(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Get current timestamp
     *
     * @return String format of timestamp
     */
    public static String now() {
        return String.valueOf(System.currentTimeMillis());
    }

}
