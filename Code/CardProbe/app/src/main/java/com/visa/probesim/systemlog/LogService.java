package com.visa.probesim.systemlog;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;

import com.visa.probesim.R;
import com.visa.probesim.Util;
import com.visa.probesim.systemcrash.GlobalException;
import com.visa.probesim.systemlog.dbase.DBContract;
import com.visa.probesim.systemlog.model.Error;
import com.visa.probesim.systemlog.model.Trace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class LogService extends IntentService {

    /**
     * Class tag for debugging
     */
    public static final String TAG = "LogService";

    /**
     * Port number used by Server Socket
     */
    public static final int PORT_NUMBER = 29800;

    /**
     * Command sent by log client to start retrieve db
     */
    public static final String CLIENT_COMMAND_QUERY_ALL = "CLIENT_COMMAND_QUERY_ALL";

    /**
     * Notification Identifier and must not be 0
     */
    private final int ONGOING_NOTIFICATION_ID = 2;

    /**
     * Notification Identifier channel
     */
    private final String CHANNEL_DEFAULT_IMPORTANCE = TAG;

    /**
     * Server socket object reference
     */
    private ServerSocket mServerSocket;

    /**
     * Intent object
     */
    private Intent mIntent;

    /**
     * Static class content broadcast status information
     */
    public static class Status {

        /**
         * Broadcast the status of CommSocket service
         */
        public static final String BROADCAST_STATUS = "com.visa.probesim.systemlog.LogService";

        /**
         * key : status
         */
        public static final String KEY_STATUS = "STATUS";

        /**
         * value : Socket stop status
         */
        public static final String STATUS_STOP = "Stop";

        /**
         * value : Waiting client status
         */
        public static final String STATUS_WAITING = "Waiting...";

        /**
         * value : Waiting client status
         */
        public static final String STATUS_CONNECTED = "Connected";

        /**
         * volatile variable of current status
         */
        private static String mStatus = STATUS_STOP;

        /**
         * Store current comm socket status to shared preference
         *
         * @param status of COMM Socket
         */
        public static void store(String status) {
            mStatus = status;
        }

        /**
         * Get current comm socket status.
         *
         * @return current status
         */
        public static String current() {
            return mStatus;
        }
    }

    /**
     * Default constructor
     */
    public LogService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mIntent = new Intent(LogService.Status.BROADCAST_STATUS);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Util.Log(TAG, "Start LogService");
        showNotification();
        startServerSocket();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Util.warnUser("System Log Start", this);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Util.Log(TAG, "System Log Stop");
        Util.warnUser("System Log Stop", this);
        broadcastStatus(LogService.Status.STATUS_STOP);
        try {
            if (mServerSocket != null) mServerSocket.close();
        } catch (IOException error) {
            throw GlobalException.build(error);
        }
        super.onDestroy();
    }

    /**
     * Start foreground notification so that the service becomes foreground service. Foreground
     * service is best to run object in the long run as android will not kill it when the
     * system has low memory.
     */
    private void showNotification() {

        Intent intent = new Intent(this, LogActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification =
                new NotificationCompat.Builder(this, CHANNEL_DEFAULT_IMPORTANCE)
                        .setContentTitle(getText(R.string.logservice_notification_title))
                        .setContentText(getText(R.string.logservice_notification_message))
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentIntent(pIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setPriority(Notification.PRIORITY_LOW)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    /**
     * Start Socket server object
     */
    private void startServerSocket() {

        String clientCommand;

        try {

            mServerSocket = new ServerSocket(PORT_NUMBER);
            Util.Log(TAG, "ServerSocket listen to PORT : " + PORT_NUMBER);
            broadcastStatus(LogService.Status.STATUS_WAITING);

            Socket socket = mServerSocket.accept();
            broadcastStatus(LogService.Status.STATUS_CONNECTED);

            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = socket.getOutputStream();
            PrintStream printer = new PrintStream(outputStream);

            while (true) {

                Util.Log(TAG, "Waiting for CLIENT_COMMAND_QUERY_ALL");

                while ((clientCommand = reader.readLine()) != null) {

                    if (clientCommand.equals(CLIENT_COMMAND_QUERY_ALL)) {

                        Util.Log(TAG, "Received CLIENT_COMMAND_QUERY_ALL");

                        String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                        int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

                        printer.print("@@@@ VERSION NAME " + versionName + "\n");
                        printer.print("@@@@ VERSION CODE " + versionCode + "\n");

                        Cursor traces = getContentResolver().query(
                                DBContract.Trace.CONTENT_URI,
                                DBContract.Trace.projection(),
                                null,
                                null,
                                DBContract.Trace.sort()
                        );

                        if (traces != null) {
                            printer.print("@@@@ START TRACE LOG with " + traces.getCount() + " entries \n");
                            while (traces.moveToNext()) {
                                String parcel = Trace.build(traces).parcel();
                                printer.print(parcel + "\n");
                                Util.Log(TAG, "TRACE : " + parcel);
                            }
                            printer.print("@@@@ END TRACE LOG\n");
                        }

                        Cursor errors = getContentResolver().query(
                                DBContract.Error.CONTENT_URI,
                                DBContract.Error.projection(),
                                null,
                                null,
                                DBContract.Error.sort()
                        );

                        if (errors != null) {
                            printer.print("@@@@ START ERROR LOG with " + errors.getCount() + " entries \n");
                            while (errors.moveToNext()) {
                                String parcel = Error.build(errors).parcel();
                                printer.print(parcel + "\n");
                                Util.Log(TAG, "ERROR : " + parcel);
                            }
                            printer.print("@@@@ END ERROR LOG\n");
                        }
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException error) {
            throw GlobalException.build(error);
        } catch (IOException error) {
            this.stopSelf();
        }
    }

    /**
     * Send broadcast to main-activity showing the socket status to user
     *
     * @param status of socket server
     */
    private void broadcastStatus(String status) {
        LogService.Status.store(status);
        mIntent.putExtra(LogService.Status.KEY_STATUS, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
    }
}
