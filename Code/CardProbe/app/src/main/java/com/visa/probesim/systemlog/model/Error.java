package com.visa.probesim.systemlog.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.visa.probesim.systemlog.dbase.DBContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Error {

    /**
     * Error Type contain information of exception type
     */
    private String mType;

    /**
     * Local time taken when the error happened
     */
    private String mTimestamp;

    /**
     * Error Stack trace information
     */
    private String mContent;

    /**
     * Private constructor. Only access by build() function.
     *
     * @param type of error
     * @param timestamp of error
     * @param content of error
     */
    private Error(String type, String timestamp, String content) {
        this.mType = type;
        this.mTimestamp = timestamp;
        this.mContent = content;
    }

    /**
     * Build error object to be stored to SQLite database
     *
     * @param type of error (exception type)
     * @param timestamp of trace
     * @param content of trace
     * @return trace object
     */
    public static Error build(String type, String timestamp, String content) {
        return new Error(type, timestamp, content);
    }

    /**
     * Build trace object from cursor object
     *
     * @param cursor trace object
     * @return trace object
     */
    public static Error build(Cursor cursor) {
        return new Error(
                cursor.getString(cursor.getColumnIndex(DBContract.Error.COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(DBContract.Error.COLUMN_TIMESTAMP)),
                cursor.getString(cursor.getColumnIndex(DBContract.Error.COLUMN_CONTENT))
        );
    }

    /**
     * Build parcel object from trace object. Parcel object used to send information easily to
     * client reading db information.
     *
     * @return parcel format of trace object
     */
    public String parcel() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(mTimestamp));
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
        String timestamp = fmt.format(calendar.getTime());
        return (new StringBuilder(timestamp).append("|").append(mType).append("|").append(mContent)).toString();
    }

    /**
     * Store trace object to Trace Table on content provider
     *
     * @param context of application
     */
    public void store(Context context) {
        ContentValues values = new ContentValues();
        values.put(DBContract.Error.COLUMN_TYPE, mType);
        values.put(DBContract.Error.COLUMN_TIMESTAMP, mTimestamp);
        values.put(DBContract.Error.COLUMN_CONTENT, mContent);
        context.getContentResolver().insert(DBContract.Error.CONTENT_URI, values);
    }

    /**
     * Clear all trace data from table
     *
     * @param context of application
     */
    public static void clearTable(Context context) {
        context.getContentResolver().delete(DBContract.Error.CONTENT_URI, null, null);
    }
}
