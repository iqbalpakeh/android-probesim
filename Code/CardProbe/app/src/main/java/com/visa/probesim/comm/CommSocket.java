package com.visa.probesim.comm;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;

import com.visa.probesim.MainActivity;
import com.visa.probesim.R;
import com.visa.probesim.Util;
import com.visa.probesim.systemcrash.GlobalException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class CommSocket extends IntentService {

    /**
     * Class tag for debugging
     */
    public static final String TAG = "CommSocket";

    /**
     * Port number used by Server Socket
     */
    public static final int PORT_NUMBER = 29700;

    /**
     * Notification Identifier and must not be 0
     */
    private final int ONGOING_NOTIFICATION_ID = 1;

    /**
     * Notification Identifier channel
     */
    private final String CHANNEL_DEFAULT_IMPORTANCE = TAG;

    /**
     * Server socket object reference
     */
    private ServerSocket mServerSocket;

    /**
     * Intent object
     */
    private Intent mIntent;

    /**
     * Static class content broadcast status information
     */
    public static class Status {

        /**
         * Broadcast the status of CommSocket service
         */
        public static final String BROADCAST_STATUS = "com.visa.probesim.comm.CommSocket";

        /**
         * key : status
         */
        public static final String KEY_STATUS = "STATUS";

        /**
         * value : Socket stop status
         */
        public static final String STATUS_STOP = "Stop";

        /**
         * value : Waiting client status
         */
        public static final String STATUS_WAITING = "Waiting...";

        /**
         * value : Waiting client status
         */
        public static final String STATUS_CONNECTED = "Connected";

        /**
         * volatile variable of current status
         */
        private static String mStatus = STATUS_STOP;

        /**
         * Store current comm socket status to shared preference
         *
         * @param status of COMM Socket
         */
        public synchronized static void store(String status) {
            mStatus = status;
        }

        /**
         * Get current comm socket status.
         *
         * @return current status
         */
        public synchronized static String current() {
            return mStatus;
        }
    }

    /**
     * Default constructor
     */
    public CommSocket() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mIntent = new Intent(Status.BROADCAST_STATUS);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Util.warnUser("ProbeSIM Start", this);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Util.Log(TAG, "Start ProbeSIM");
        showNotification();
        startServerSocket();
    }

    @Override
    public void onDestroy() {
        Util.Log(TAG, "ProbeSIM Stop");
        Util.warnUser("ProbeSIM Stop", this);
        broadcastStatus(Status.STATUS_STOP);
        try {
            IOBuffer.getInstance().destroyBuffer();
            if (mServerSocket != null) mServerSocket.close();
        } catch (IOException error) {
            throw GlobalException.build(error);
        }
        super.onDestroy();
    }

    /**
     * Start foreground notification so that the service becomes foreground service. Foreground
     * service is best to run object in the long run as android will not kill it when the
     * system has low memory.
     */
    private void showNotification() {

        Intent intent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification =
                new NotificationCompat.Builder(this, CHANNEL_DEFAULT_IMPORTANCE)
                        .setContentTitle(getText(R.string.commsocket_notification_title))
                        .setContentText(getText(R.string.commsocket_notification_message))
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentIntent(pIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    /**
     * Start Socket server object
     */
    private void startServerSocket() {

        String response;

        try {

            mServerSocket = new ServerSocket(PORT_NUMBER);
            Util.Log(TAG, "ServerSocket listen to PORT : " + PORT_NUMBER);
            broadcastStatus(Status.STATUS_WAITING);

            Socket socket = mServerSocket.accept();
            broadcastStatus(Status.STATUS_CONNECTED);

            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = socket.getOutputStream();
            PrintStream printer = new PrintStream(outputStream);

            while (true) {

                Util.Log(TAG, "Waiting IOBuffer...");
                String apdu = IOBuffer.getInstance().takeApduCommand();

                printer.print(prepareApdu(apdu));
                Util.Log(TAG, "APDU Command : " + apdu);

                while ((response = reader.readLine()) != null) {
                    String prepResponse = prepareResponse(response);
                    IOBuffer.getInstance().putApduResponse(prepResponse);
                    Util.Log(TAG, "APDU Response : " + prepResponse);
                    break;
                }
            }
        } catch (IOException error) {
            this.stopSelf();
        }
    }

    /**
     * Send broadcast to main-activity showing the socket status to user
     *
     * @param status of socket server
     */
    private void broadcastStatus(String status) {
        Status.store(status);
        mIntent.putExtra(Status.KEY_STATUS, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
    }

    /**
     * Adding prefix come from POS Terminal before sending to ARTS.
     * Format to ARTS:
     * 02000000 + LL000000 + APDU
     *
     * @param apdu from POS Terminal
     * @return prepared apdu
     */
    private String prepareApdu(String apdu) {
        StringBuilder sb = new StringBuilder();
        int lengthInt = apdu.length() / 2;
        String lengthByte = Integer.toHexString(lengthInt);
        sb.append("02000000");
        if (lengthInt < 0x10) sb.append("0").append(lengthByte).append("000000");
        else sb.append(lengthByte).append("000000");
        sb.append(apdu);
        return sb.toString();
    }

    /**
     * Removing prefix come from ARTS before sending to POS Terminal
     * Format from ARTS:
     * LL000000 + APDU
     *
     * @param response from ARTS
     * @return apdu response without prefix
     */
    private String prepareResponse(String response) {
        StringBuilder sb = new StringBuilder(response);
        sb.delete(0, 8);
        return sb.toString();
    }
}
