package com.visa.probesim.systemlog.dbase;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DBContract {

    /**
     * Content Authority
     */
    public static final String CONTENT_AUTHORITY = "com.visa.probesim.systemlog.dbase";

    /**
     * Base URI to access content provider
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Empty private constructor to stop any unwanted object creation
     */
    private DBContract() {

    }

    /**
     * Constant used by Trace table
     */
    public static final class Trace implements BaseColumns {

        /**
         * Name of database transaction table
         */
        public final static String TABLE_NAME = "TRACE_TABLE";

        /**
         * Unique ID number (only for use in the database table).
         * Type: INTEGER
         */
        public final static String COLUMN_ID = BaseColumns._ID;

        /**
         * Trace type
         *      1. COMMAND
         *      2. RESPONSE
         * Type: TEXT
         */
        public final static String COLUMN_TYPE = "TYPE";

        /**
         * Trace timestamp
         * Type: TEXT
         */
        public final static String COLUMN_TIMESTAMP = "TIMESTAMP";

        /**
         * Trace content
         * Type: TEXT
         */
        public final static String COLUMN_CONTENT = "CONTENT";

        /**
         * Create CONTENT_URI
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        /**
         * Builds URIs on insertion
         *
         * @param id of selected row
         * @return concatenated uri
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        /**
         * Projection used by Query
         *
         * @return columns projection
         */
        public static String[] projection() {
            return new String[]{
                    COLUMN_ID,
                    COLUMN_TYPE,
                    COLUMN_TIMESTAMP,
                    COLUMN_CONTENT
            };
        }

        /**
         * Sort mode used by Query
         *
         * @return SQL sorted mode
         */
        public static String sort() {
            return COLUMN_TIMESTAMP + " ASC ";
        }
    }

    /**
     * Constant used by Error table
     */
    public static final class Error implements BaseColumns {

        /**
         * Name of database transaction table
         */
        public final static String TABLE_NAME = "ERROR_TABLE";

        /**
         * Unique ID number (only for use in the database table).
         * Type: INTEGER
         */
        public final static String COLUMN_ID = BaseColumns._ID;

        /**
         * Error type (exception name)
         * Type: TEXT
         */
        public final static String COLUMN_TYPE = "TYPE";

        /**
         * Error timestamp
         * Type: TEXT
         */
        public final static String COLUMN_TIMESTAMP = "TIMESTAMP";

        /**
         * Error content
         * Type: TEXT
         */
        public final static String COLUMN_CONTENT = "CONTENT";

        /**
         * Create CONTENT_URI
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        /**
         * Builds URIs on insertion
         *
         * @param id of selected row
         * @return concatenated uri
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        /**
         * Projection used by Query
         *
         * @return columns projection
         */
        public static String[] projection() {
            return new String[]{
                    COLUMN_ID,
                    COLUMN_TYPE,
                    COLUMN_TIMESTAMP,
                    COLUMN_CONTENT
            };
        }

        /**
         * Sort mode used by Query
         *
         * @return SQL sorted mode
         */
        public static String sort() {
            return COLUMN_TIMESTAMP + " ASC ";
        }
    }
}
