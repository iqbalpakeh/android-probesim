package com.visa.probesim.systemcrash;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.visa.probesim.Util;
import com.visa.probesim.systemlog.model.Error;

import java.io.PrintWriter;
import java.io.StringWriter;

public class App extends Application {

    public static final String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {

                StringWriter error = new StringWriter();
                e.printStackTrace(new PrintWriter(error));
                Util.Log(TAG, "CRASH : " + error.toString());

                Error.build(Thread.UncaughtExceptionHandler.class.getSimpleName(),
                        Util.now(), error.toString()).store(App.this);

                Intent intent = new Intent(App.this, CrashActivity.class);
                intent.putExtra("CRASH", error.toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        App.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                AlarmManager manager = (AlarmManager) App.this.getSystemService(Context.ALARM_SERVICE);
                manager.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
                System.exit(0);
            }
        });
    }
}
