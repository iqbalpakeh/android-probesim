package com.visa.probesim;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.visa.probesim.comm.CommNfc;
import com.visa.probesim.comm.CommSocket;
import com.visa.probesim.systemlog.LogActivity;
import com.visa.probesim.systemlog.LogStatus;
import com.visa.probesim.systemlog.model.Error;
import com.visa.probesim.systemlog.model.Trace;
import com.wang.avi.AVLoadingIndicatorView;

public class MainActivity extends AppCompatActivity {

    //todo: Update UI using VISA provided resource file

    /**
     * Class tag for debugging
     */
    public static final String TAG = "MainActivity";

    /**
     * Text showing the socket status
     */
    private TextView mSocketStatus;

    /**
     * Text showing ip address socket listening to
     */
    private TextView mIpAddressText;

    /**
     * Local IPAddress provided by the network
     */
    private String mIpAddress;

    /**
     * Checkbox to show status of log apdus
     */
    private CheckBox mCheckBox;

    /**
     * Active APDU indicator
     */
    private AVLoadingIndicatorView mIndicatorActive;

    /**
     * Idle APDU indicator
     */
    private AVLoadingIndicatorView mIndicatorIdle;

    /**
     * Navigation button listener. Used to start or stop socket service
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_start:
                    handleStartButton();
                    return true;
                case R.id.navigation_stop:
                    handleStopButton();
                    return true;
            }
            return false;
        }
    };

    /**
     * Handle start button controlled by user
     */
    private void handleStartButton() {
        if (CommSocket.Status.current().equals(CommSocket.Status.STATUS_STOP)) {
            startService(new Intent(MainActivity.this, CommSocket.class));
        } else {
            Util.warnUser("ProbeSIM is already started", MainActivity.this);
        }
    }

    /**
     * Handle stop button controlled by user
     */
    private void handleStopButton() {
        if (!CommSocket.Status.current().equals(CommSocket.Status.STATUS_STOP)) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Stop ProbeSIM ?")
                    .setMessage("TCP/IP connection will be closed")
                    .setIcon(R.drawable.ic_warning_orange_24dp)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            stopService(new Intent(MainActivity.this, CommSocket.class));
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        } else {
            Util.warnUser("ProbeSIM is not started yet", MainActivity.this);
        }
    }

    /**
     * Receive any message from socket service status broadcast
     */
    private BroadcastReceiver mSocketReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra(CommSocket.Status.KEY_STATUS);
            Util.Log(TAG, "Socket server status = " + status);
            mSocketStatus.setText(status);
            if (status.equals(CommSocket.Status.STATUS_CONNECTED)) {
                mIndicatorActive.setVisibility(View.INVISIBLE);
                mIndicatorIdle.setVisibility(View.VISIBLE);
            } else {
                mIndicatorActive.setVisibility(View.INVISIBLE);
                mIndicatorIdle.setVisibility(View.INVISIBLE);
            }
        }
    };

    /**
     * Receive any message from NFC Service broadcast
     */
    private BroadcastReceiver mNfcReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra(CommNfc.Status.KEY_STATUS);
            Util.Log(TAG, "CommNfc status = " + status);
            if (status.equals(CommNfc.Status.STATUS_ACTIVE)) {
                mIndicatorActive.setVisibility(View.VISIBLE);
                mIndicatorIdle.setVisibility(View.INVISIBLE);
            } else if (status.equals(CommNfc.Status.STATUS_IDLE)) {
                mIndicatorActive.setVisibility(View.INVISIBLE);
                mIndicatorIdle.setVisibility(View.VISIBLE);
            } else throw new IllegalArgumentException("Illegal broadcast status from CommNfc");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIpAddress = Util.getIpAddress();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mSocketStatus = findViewById(R.id.socket_status);
        mIpAddressText = findViewById(R.id.ip_address);
        mIndicatorActive = findViewById(R.id.apdu_indicator_active);
        mIndicatorIdle = findViewById(R.id.apdu_indicator_idle);
        mCheckBox = findViewById(R.id.cb_log_apdus);

        // Show the Socket status and IPAddress
        mSocketStatus.setText(CommSocket.Status.current());
        mIpAddressText.setText(new StringBuilder(mIpAddress).append(" : ").append(CommSocket.PORT_NUMBER));

        // Check the status of CommSocket when MainActivity start.
        // Shows the current status of client indicator
        if (CommSocket.Status.current().equals(CommSocket.Status.STATUS_CONNECTED)) {
            if (CommNfc.Status.current().equals(CommNfc.Status.STATUS_ACTIVE)) {
                mIndicatorActive.setVisibility(View.VISIBLE);
                mIndicatorIdle.setVisibility(View.INVISIBLE);
            } else {
                mIndicatorActive.setVisibility(View.INVISIBLE);
                mIndicatorIdle.setVisibility(View.VISIBLE);
            }
        } else {
            mIndicatorActive.setVisibility(View.INVISIBLE);
            // mIndicatorIdle.setVisibility(View.INVISIBLE);
        }

        // Check the status of CommNfc when MainActivity start.
        // Shows the current status of log indicator
        if (LogStatus.current(this).equals(LogStatus.STATUS_LOG))
            mCheckBox.setChecked(true);
        else mCheckBox.setChecked(false);

        // Checkbox listener toggling system log feature
        mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked())
                    LogStatus.store(MainActivity.this, LogStatus.STATUS_LOG);
                else LogStatus.store(MainActivity.this, LogStatus.STATUS_NO_LOG);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        instance.registerReceiver(mSocketReceiver, new IntentFilter(CommSocket.Status.BROADCAST_STATUS));
        instance.registerReceiver(mNfcReceiver, new IntentFilter(CommNfc.Status.BROADCAST_STATUS));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        instance.unregisterReceiver(mSocketReceiver);
        instance.unregisterReceiver(mNfcReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_retrieve_log) {
            stopService(new Intent(MainActivity.this, CommSocket.class));
            Intent startSystemLog = new Intent(this, LogActivity.class);
            startSystemLog.putExtra(LogActivity.EXTRA_IPADDRESS, mIpAddress);
            startActivity(startSystemLog);
            return true;
        } else if (id == R.id.action_clear_apdu_log) {
            Trace.clearTable(this);
            Util.warnUser(getString(R.string.message_apdu_log_cleared), this);
            return true;
        } else if (id == R.id.action_clear_error_log) {
            Error.clearTable(this);
            Util.warnUser(getString(R.string.message_error_log_cleared), this);
            return true;
        } else if (id == R.id.action_about) {
            Intent startAboutActivity = new Intent(this, AboutActivity.class);
            startActivity(startAboutActivity);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
